package com.example.imt202111tpspringkumar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Imt202111TpSpringKumarApplication {

    public static void main(String[] args) {
        SpringApplication.run(Imt202111TpSpringKumarApplication.class, args);
    }

}
